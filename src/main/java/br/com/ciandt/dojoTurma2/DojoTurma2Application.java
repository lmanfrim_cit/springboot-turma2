package br.com.ciandt.dojoTurma2;


import br.com.ciandt.dojoTurma2.dao.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DojoTurma2Application {

	@Autowired
	private ClienteRepository clienteRepository;

	public static void main(String[] args) {
		SpringApplication.run(DojoTurma2Application.class);
	}

}

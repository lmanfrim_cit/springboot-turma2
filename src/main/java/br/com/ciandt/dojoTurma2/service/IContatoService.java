package br.com.ciandt.dojoTurma2.service;

import br.com.ciandt.dojoTurma2.dao.ContatoRepository;
import br.com.ciandt.dojoTurma2.entity.Contato;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface IContatoService {

    List<Contato> findAll();

    List<Contato> findByNome(String nome);

    List<Contato> findBySistema(String sistema);




}

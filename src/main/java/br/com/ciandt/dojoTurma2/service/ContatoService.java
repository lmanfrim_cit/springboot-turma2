package br.com.ciandt.dojoTurma2.service;

import br.com.ciandt.dojoTurma2.dao.ContatoRepository;
import br.com.ciandt.dojoTurma2.entity.Contato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService implements IContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    @Override
    public List<Contato> findByNome(String nome) {
        return contatoRepository.findByNome(nome);
    }

    @Override
    public List<Contato> findBySistema(String sistema) {
        return contatoRepository.findBySistema(sistema);
    }

    public List<Contato> findAll() {
        return contatoRepository.findAll();
    }
}

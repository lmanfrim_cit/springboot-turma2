package br.com.ciandt.dojoTurma2.dao;

import br.com.ciandt.dojoTurma2.entity.Contato;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContatoRepository extends JpaRepository<Contato, Long> {
    List<Contato> findByNome(String nome);
    List<Contato> findBySistema(String sistema);
}


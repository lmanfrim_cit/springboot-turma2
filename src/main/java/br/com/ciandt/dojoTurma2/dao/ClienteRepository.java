package br.com.ciandt.dojoTurma2.dao;

import br.com.ciandt.dojoTurma2.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ClienteRepository extends JpaRepository<Cliente, Long>{
}

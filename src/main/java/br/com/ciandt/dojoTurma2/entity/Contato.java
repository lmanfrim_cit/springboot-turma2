package br.com.ciandt.dojoTurma2.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Contato {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

    private String area;

    private String local;

    private String sistema;

    private String email;

    @OneToMany
    private List<Telefone> telefones;
}

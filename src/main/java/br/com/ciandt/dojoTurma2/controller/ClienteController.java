package br.com.ciandt.dojoTurma2.controller;

import br.com.ciandt.dojoTurma2.dao.ClienteRepository;
import br.com.ciandt.dojoTurma2.entity.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cliente")
public class ClienteController  {

    @Autowired
    ClienteRepository repository;

    @GetMapping
    public List<Cliente> clientes() {
        return repository.findAll();
    }




}

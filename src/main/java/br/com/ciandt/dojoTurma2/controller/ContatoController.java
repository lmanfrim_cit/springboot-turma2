package br.com.ciandt.dojoTurma2.controller;

import br.com.ciandt.dojoTurma2.entity.Cliente;
import br.com.ciandt.dojoTurma2.entity.Contato;
import br.com.ciandt.dojoTurma2.service.ContatoService;
import br.com.ciandt.dojoTurma2.service.IContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/contato")
public class ContatoController {

    @Autowired
    IContatoService contatoService;

    @GetMapping
    public List<Contato> contatos() {
        return contatoService.findAll();
    }

    @GetMapping("/{nome}")
    public List<Contato> contatoByNome(@RequestParam String nome) {
        return contatoService.findByNome(nome);
    }

    @GetMapping("/sistema/{nome}")
    public List<Contato> contatoBySistema(@RequestParam String nome) {
        return contatoService.findBySistema(nome);
    }

}
